#!/usr/bin/env bash

flask db migrate
flask db upgrade

exec flask run --host=0.0.0.0
