# Year2271

## Requirements

* [x] Create a Python (>= 3.8) web application using Flask framework and implement endpoints presented below
* [x] The application is covered by unit and integration tests using pytest
* [x] Local deployment, tests and others are run using Docker/docker-compose
* [x] Repository contains README with local deployment and tests instruction
* [x] Application uses PostgreSQL as database
* [ ] Codebase is typed and commented
* [x] Repository is pushed to Github and link is provided to the recruiter
* [x] Error handling and edge cases


## Local setup

Clone the repo:

```bash
$ git clone git@gitlab.com:robley/year2271.git
$ cd year2271
```

Ensure you have Python 3.6 installed. On a mac, use `brew install python@3.6`.

Create and activate a virtualenv and install dependencies:

```bash
$ python3.6 -m venv env
$ source env/bin/activate
$ pip install -r requirements.txt
```

Ensure you have [PostgreSQL](https://www.postgresql.org/) installed and running. Create a postgresql user with username and password `year2271`,
and create a corresponding database called `year2271` and a test database called `test_year2271`.

```bash
$ sudo su - postgres -c 'createuser -d -P year2271'
$ sudo su - postgres -c 'createdb year2271'

$ sudo su - postgres -c 'createdb test_year2271'
```

Check that you can connect to the postgresql database as your regular shell user (not year2271 user) by means of password authentication:

```bash
$ psql -h localhost year2271 year2271
```

If you can't connect, you can modify your `pg_hba.conf` (`/etc/postgresql/9.6/main/pg_hba.conf` for postgresql 9.6) to allow md5 encrypted password authentication for users on localhost by adding a line like this:

```
local	all		all     md5
```

Then run migrations to setup the initial database:

```bash
$ flask db migrate
$ flask db upgrade
```

Set the `DATABASE_URL` in the `.env` file to point to the newly created Postgres database.

Start the app:
```bash
$ source .env
$ flask run
```

### Testing

Set the `DATABASE_URL` in the `.env` file to point to the test Postgres database `test_year2271`.

Run the tests:
```bash
$ source .env
$ pytest -v
```

## Running in Docker

Run the app in Docker with docker-compose:
```
$ docker-compose up
```

Using your api client, direct your requests to `127.0.0.1/api/v1/games`

### Testing

Run Tests in Docker with docker-compose:
```
$ docker-compose -f docker-compose.tests.yml up --abort-on-container-exit --exit-code-from test_web 
```

## Implemented Endpoints

1. GET `/api/v1/status`

   - returns appropriate status code and {"database": "healthy"} when the database connection is healthy
   - returns appropriate status code and {"database": "unhealthy"} when the database connection isn't healthy

2. HEAD `/api/v1/status`

   - Returns appropriate status code when the database connection is healthy
   - Returns appropriate status code when the database connection isn't healthy

3. POST `/api/v1/games`

   - Validates the payload and saves the game into the DB
   - Request payload schema:

   ```json
   {
     "name": "Diablo 112",
     "price": 71.7,
     "space": 1073741824
   }
   ```

   - Success sample response payload:

   ```json
   {
     "name": "Diablo 112",
     "price": 71.7,
     "space": 1073741824
   }
   ```

   - For success and failure return appropriate status codes

4. POST `/api/v1/best_value_games?pen_drive_space={POSITIVE_INTEGER}`
   - Return a combination of games that has the highest total value of all possible game combinations 
     that fits given pen-drive space
   - Validate pen_drive_space query parameter
   - Success sample response payload:
   ```json
   {
     "games": [
       {
         "name": "Super Game",
         "price": 71.7,
         "space": 1073741824
       },
       {
         "name": "Extra Game",
         "price": 100.78,
         "space": 2147483648
       }
     ],
     "total_space": 3221225472,
     "remaining_space": 1024,
     "total_value": 172.48
   }
   ```
