import os
from flask import Flask
from flask_migrate import Migrate

from year2271.models import db
from year2271.config import config_dict


def create_app(config_obj):
    app = Flask(__name__)
    app.config.from_object(config_dict.get(config_obj, 'default'))

    db.init_app(app)
    migrate = Migrate()
    migrate.init_app(app, db)

    register_blueprints(app)

    return app


def register_blueprints(app):
    from year2271.routes import default_blueprint

    app.register_blueprint(default_blueprint)
