from typing import Dict, Any
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Game(db.Model):
    __tablename__ = 'games'

    name = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)  # unique, not empty string
    price = db.Column(db.Float())  # non-negative float
    space = db.Column(db.Integer())  # positive (1073741824 - 1 GB in bytes)

    def __init__(self, name: str, price: int, space: int) -> None:
        self.name = name
        self.price = price
        self.space = space

    def __repr__(self) -> str:
        return f'<{self.name}>'


def calculate_best_value_games(space_from_request: int) -> Dict[str, Any]:
    """
    # get all games smaller than total pen drive space
    # sort by price
    # get the top that can fit into pen drive space
    """
    flt = space_from_request
    games = db.session.query(Game).filter(Game.space <= flt).order_by(Game.price.desc()).all()

    new_games = []
    total_space = 0
    total_value = 0
    remaining_space = flt

    for game in games:
        flt -= game.space
        if flt >= 0:
            new_games.append(game)
            total_space += game.space
            total_value += game.price
            remaining_space = flt

    data = {
        "games": [
            {
                "name": g.name,
                "price": g.price,
                "space": g.space
            } for g in new_games
         ],
        "total_space": total_space,
        "remaining_space": remaining_space,
        "total_value": total_value,
    }
    return data
