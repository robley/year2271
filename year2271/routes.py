from flask import Blueprint, jsonify, request

from year2271.models import db, Game, calculate_best_value_games


default_blueprint = Blueprint('games', __name__)


@default_blueprint.route('/api/v1/status', methods=['GET', 'HEAD'])
def database_status():
    """
    - returns appropriate status code and {"database": "healthy"} when the database connection is healthy
    - returns appropriate status code and {"database": "unhealthy"} when the database connection isn't healthy
    """
    data = {
        "database": None
    }
    try:
        db.engine.execute("SELECT 1")
        data['database'] = 'healthy'
        status = 200
    except Exception as e:
        data['database'] = 'unhealthy'
        status = 500

    return jsonify(data), status


@default_blueprint.route('/api/v1/games', methods=['POST'])
def save_game():
    """
    Validates the payload and saves the game into the DB
    """
    name = request.json.get('name')

    try:
        price = float(request.json.get('price'))
        if price < 0:
            resp = {"message": "Price should be a positive float value"}
            return jsonify(resp), 400
    except ValueError as e:
        resp = {"message": "Price should be a positive float value"}
        return jsonify(resp), 400

    try:
        space = int(request.json.get('space'))
        if space < 0:
            resp = {"message": "Space should be a positive integer"}
            return jsonify(resp), 400
    except ValueError as e:
        resp = {"message": "Space should be a positive float value"}
        return jsonify(resp), 400

    if not name:
        resp = {"message": "Name cannot be empty"}
        return jsonify(resp), 400

    new_game = Game(name, price, space)
    if not db.session.query(Game).filter_by(name=request.json.get('name')).first():
        db.session.add(new_game)
        db.session.commit()
        status_code = 200
        resp = {'name': new_game.name, 'price': new_game.price, 'space': new_game.space}
    else:
        status_code = 409
        resp = {"message": f"{request.json.get('name')} already exists"}

    return jsonify(resp), status_code


@default_blueprint.route('/api/v1/best_value_games', methods=['GET', 'POST'])
def best_value_games():
    """
    - Return a combination of games that has the highest total value of all possible game combinations
     that fits given pen-drive space

    """
    try:
        pen_drive_space = int(request.args.get('pen_drive_space'))
        if pen_drive_space and pen_drive_space > 0:
            resp = calculate_best_value_games(pen_drive_space)
            status_code = 200

        else:
            status_code = 400
            resp = {'error': f"{request.args.get('pen_drive_space')} is not a positive integer"}

        return jsonify(resp), status_code

    except ValueError:
        status_code = 400
        resp = {'error': f"{request.args.get('pen_drive_space')} is not a positive integer"}

        return jsonify(resp), status_code
