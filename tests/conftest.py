import pytest
from year2271 import create_app
from year2271.models import db, Game


@pytest.fixture(scope='module')
def test_app():
    return create_app('testing')


@pytest.fixture(scope='module')
def test_client(test_app):
    with test_app.test_client() as test_client:
        with test_app.app_context():
            yield test_client


@pytest.fixture(scope='module')
def init_database(test_app, test_client):
    db.init_app(test_app)
    db.create_all()

    g1 = Game('Fortnite 44', 44.7, 1441824)
    g2 = Game('Fortnite 43', 43.7, 1431826)
    g3 = Game('Fortnite 45', 45.7, 1451879)

    db.session.add(g1)
    db.session.add(g2)
    db.session.add(g3)

    db.session.commit()

    yield

    db.close_all_sessions()
    db.drop_all()


@pytest.fixture(scope='module')
def new_game():
    return Game('Fortnite 44', 44.7, 1441824)
