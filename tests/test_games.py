import json


def test_new_game(new_game):
    assert new_game.name == 'Fortnite 44'
    assert new_game.price == 44.7
    assert new_game.space == 1441824


# test save game with correct details
def test_save_game(test_client, init_database):
    response = test_client.post('/api/v1/games',
                                data=json.dumps({
                                    "name": "Fort",
                                    "price": 71.7,
                                    "space": 1073741824
                                }),
                                content_type='application/json'
                                )

    json_data = response.get_json()
    assert response.status_code == 200
    assert json_data.get('name') == "Fort"
    assert json_data.get('price') == 71.7
    assert json_data.get('space') == 1073741824


# test save existing game
def test_existing_game(test_client, init_database):
    response = test_client.post('/api/v1/games',
                                data=json.dumps({
                                    "name": "Fortnite 44",
                                    "price": 9.7,
                                    "space": 1073741824
                                }),
                                content_type='application/json'
                                )

    assert response.status_code == 409


# test save game with negative price
def test_save_negative_price(test_client, init_database):
    response = test_client.post('/api/v1/games',
                                data=json.dumps({
                                    "name": "Fortnite",
                                    "price": -9,
                                    "space": 1073741824
                                }),
                                content_type='application/json'
                                )

    assert response.status_code == 400


# test save game with negative space
def test_save_negative_space(test_client, init_database):
    response = test_client.post('/api/v1/games',
                                data=json.dumps({
                                    "name": "Fortnite",
                                    "price": 9,
                                    "space": -1234
                                }),
                                content_type='application/json'
                                )

    assert response.status_code == 400


# test save game with string space
def test_save_string_space(test_client, init_database):
    response = test_client.post('/api/v1/games',
                                data=json.dumps({
                                    "name": "Fortnite",
                                    "price": 123,
                                    "space": "abc"
                                }),
                                content_type='application/json'
                                )

    assert response.status_code == 400


# test save game with string price
def test_save_string_price(test_client, init_database):
    response = test_client.post('/api/v1/games',
                                data=json.dumps({
                                    "name": "Fortnite",
                                    "price": "abc",
                                    "space": 1234
                                }),
                                content_type='application/json'
                                )

    assert response.status_code == 400


# test get best value games based on test data - check for names
def test_best_value_games(test_client, init_database):
    response = test_client.post('/api/v1/best_value_games?pen_drive_space=1441824')
    json_data = response.get_json()
    games = json_data.get('games')

    assert response.status_code == 200

    assert len(games) == 1
    assert games[0]['name'] == 'Fortnite 44'
    assert games[0]['price'] == 44.7

    assert json_data.get('remaining_space') == 0
    assert json_data.get('total_space') == 1441824
    assert json_data.get('total_value') == 44.7


# test when no game can be found - remaining space should be equal to space provided
def test_no_best_value_game(test_client, init_database):
    response = test_client.post('/api/v1/best_value_games?pen_drive_space=1411823')
    json_data = response.get_json()
    games = json_data.get('games')

    assert response.status_code == 200

    assert len(games) == 0
    assert json_data.get('remaining_space') == 1411823
    assert json_data.get('total_space') == 0
    assert json_data.get('total_value') == 0
