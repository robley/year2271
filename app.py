import os

from year2271 import create_app


app = create_app(os.getenv('FLASK_ENV', 'development'))
